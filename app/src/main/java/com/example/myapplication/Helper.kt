package com.example.myapplication

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class Helper(context : Context) : SQLiteOpenHelper(context, "database.db", null, 1) {
    val patInfo = " CREATE TABLE IF NOT EXISTS " +
            "login(" +
            " acc TEXT," +
            " pas TEXT" +
            ", PRIMARY KEY(acc))"

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(patInfo)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        TODO("Not yet implemented")
    }

}