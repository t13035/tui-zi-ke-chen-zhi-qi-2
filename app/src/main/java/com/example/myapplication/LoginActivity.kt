package com.example.myapplication

import android.content.Intent
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.appcompat.app.AppCompatActivity
import android.widget.ArrayAdapter;
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        var personHelper = PersonHelper(this)
        var SQL = SQLite(applicationContext)
        personHelper.onCreate(personHelper.writableDatabase)

        var personData = SQL.queryData("person", "ID, name, age, sex"," 1 = 1")
        listview.adapter = MyAdapter(this)
        listview.setOnItemClickListener { parent, view, position, id ->

            AlertDialog.Builder(this)
                .setTitle("確認視窗")
                .setMessage("請選擇您要執行的動作")
                .setNegativeButton("修改") {_,_ ->
                    val intent = Intent(this, UpdPersonActivity::class.java)
                    startActivity(intent)
                }
                .setPositiveButton("取消", null)
                .setNeutralButton("刪除") {_,_ ->
                    var ID = position
                    SQL.DeleteData("person", " ID = $ID")
                }
                .create()
                .show()
        }

        // 新增按鈕事件
        newDataButton.setOnClickListener {
            val intent = Intent(this, NewPersonActivity::class.java)
            startActivity(intent)
        }
    }


}