package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var Helper = Helper(this)
        var SQL = SQLite(applicationContext)
        Helper.onCreate(Helper.writableDatabase)
        login.setOnClickListener {
            var loginData = SQL.queryData("login", "acc, pas"," acc = '" + PT1.text + "' ")
            if(loginData!=null) {
                if (loginData.size!=0) {
                    if (loginData[0]["pas"]!!.equals(TPID.text.toString())) {
                        val intent = Intent(this, LoginActivity::class.java)
                        startActivity(intent)
                    } else {
                        Toast.makeText(this, "密碼錯誤!!", Toast.LENGTH_SHORT).show()
                    }
                }
            } else {
                Toast.makeText(this, "帳號不存在!!", Toast.LENGTH_SHORT).show()
            }
        }

        registered.setOnClickListener {
            var loginData = SQL.queryData("login", "acc, pas"," acc = '" + PT1.text + "' ")
            if(loginData!=null) {
                if (loginData.size != 0) {
                    Toast.makeText(this, "帳號已存在!!", Toast.LENGTH_SHORT).show()
                } else {
                    Helper.writableDatabase.execSQL("Insert into login(acc,pas)" +
                            "values ('"+ PT1.text +"','"+ TPID.text +"')")
                }
            } else {
                Helper.writableDatabase.execSQL("Insert into login(acc,pas)" +
                        "values ('"+ PT1.text +"','"+ TPID.text +"')")
            }
        }
    }
}