package com.example.myapplication

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

public class MyAdapter(context: Context) : BaseAdapter() {
    lateinit var context:Context
    lateinit var ID:Array<String>
    lateinit var Name:Array<String>
    lateinit var Age:Array<String>
    lateinit var Sex:Array<String>

    override fun getCount(): Int {
        return ID.size
    }

    override fun getItem(position: Int): Any {
        return ID[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?) : View {
        var vi = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var view = convertView ?: vi.inflate(R.layout.activity_list_view, parent, false)
        var manID = view.findViewById<TextView>(R.id.TID)
        var manName = view.findViewById<TextView>(R.id.TName)
        var manAge = view.findViewById<TextView>(R.id.TAge)
        var manSex = view.findViewById<TextView>(R.id.TSex)
        manID.text = ID.get(position)
        manName.text = Name.get(position)
        manAge.text = Age.get(position)
        manSex.text = Sex.get(position)
        return view
    }

}