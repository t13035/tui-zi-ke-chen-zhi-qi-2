package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_new_person.*

class NewPersonActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_person)
        var SQL = SQLite(applicationContext)

        updData.setOnClickListener {
            var ID = TPID.text.toString()
            var NAME = TPID.text.toString()
            var AGE = TPID.text.toString()
            var SEX = TPID.text.toString()
            SQL.insertData("person", "ID, name, age, sex"," $ID, $NAME, $AGE, $SEX")
        }

        cannelBtn.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

    }
}