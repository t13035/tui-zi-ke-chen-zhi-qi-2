package com.example.myapplication

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class PersonHelper(context : Context) : SQLiteOpenHelper(context, "database.db", null, 1) {
    val patInfo = " CREAT TABLE IF NOT EXEIST " +
            "person(" +
            " ID TEXT," +
            " name TEXT," +
            " age TEXT," +
            " sex TEXT" +
            ", PRIMARY KEY(ID))"

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(patInfo)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        TODO("Not yet implemented")
    }
}