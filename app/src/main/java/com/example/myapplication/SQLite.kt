package com.example.myapplication

import android.content.Context
import android.database.sqlite.SQLiteDatabase

class SQLite {
    lateinit var context:Context
    lateinit var db:SQLiteDatabase

    constructor(context: Context) {
        this.context = context
        db = Helper(context).writableDatabase
    }

    fun queryData(tableName: String, selectColumn:String, where:String):List<Map<String, String>>{
        var SQL = "select $selectColumn " +
                "    from $tableName "+
                "   where $where "

        val cursor = db.rawQuery(SQL, null)

        var dataList = mutableListOf<Map<String, String>>()
        if(cursor != null){
            if(cursor.count > 0){
                cursor.moveToFirst()
                var dataMap: MutableMap<String, String>
                do{
                    //清空map
                    dataMap = mutableMapOf<String, String>()
                    for(i in 0 until cursor.columnCount){
                        //將這筆資料的每個欄位名子和欄位值存入map中
                        dataMap.put(cursor.getColumnName(i), cursor.getString(i))
                    }
                    //將每筆map放入list中
                    dataList.add(dataMap)
                }while(cursor.moveToNext())
            }
        }

        return dataList
    }

    fun insertData(tableName: String, insertAddon: String, insertData: String) {
        var SQL = "insert into $tableName($insertAddon)" +
                " values ($insertData)"
        db.execSQL(SQL)
    }

    fun updateData(tableName: String, updateAddon: String, whereAddon: String) {
        var SQL = "update $tableName" +
                " set $updateAddon" +
                " where $whereAddon"
        db.execSQL(SQL)
    }

    fun DeleteData(tableName: String, deleteWhere: String) {
        var SQL = "delete from $tableName" +
                " where $deleteWhere"
        db.execSQL(SQL)
    }
}