package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_upd_person.*

class UpdPersonActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upd_person)
        var SQL = SQLite(applicationContext)

        updData.setOnClickListener {
            var ID = TPID.text.toString()
            var Name = TPName.text.toString()
            var Age = TPAge.text.toString()
            var Sex = TPSex.text.toString()
            SQL.updateData("person", "name = $Name, age = $Age, sex = $Sex"," ID = $ID")
        }

        cannelBtn.setOnClickListener{
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }
}